﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercitii_vectori
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numere = { 135, 186, 60, 28, 121, 142, 176, 167, 166, 247, 85, 164, 132, 181, 180, 149, 50, 298, 289, 192, 90, 217, 284, 72, 190, 122, 89, 183, 225, 223, 68, 250, 188, 126, 55, 292, 139, 155, 204, 273, 287, 153, 150, 123, 26, 240, 39, 9, 12, 229, 131, 100, 111, 23, 178, 66, 165, 99, 120, 156, 171, 259, 11, 74, 232, 297, 159, 84, 293, 234, 254, 19, 173, 184, 221, 158, 32, 212, 7, 210, 279, 275, 114, 77, 95, 270, 53, 281, 21, 278, 109, 1, 63, 191, 174, 218, 58, 276, 75, 41, 47, 216, 236, 103, 285, 20, 38, 300, 71, 277, 88, 86, 148, 13, 262, 203, 231, 6, 107, 228, 290, 235, 33, 5, 59, 127, 208, 206, 170, 108, 69, 286, 169, 269, 185, 233, 267, 245, 263, 162, 97, 294, 288, 200, 8, 195, 62, 35, 27, 246, 65, 22, 264, 168, 226, 25, 243, 161, 187, 40, 44, 198, 48, 136, 222, 157, 251, 272, 137, 105, 242, 43, 296, 29, 76, 94, 113, 52, 73, 2, 79, 138, 244, 205, 92, 64, 199, 133, 3, 291, 104, 102, 249, 146, 193, 36, 24, 257, 91, 81 };
            int nrMax = 0;
            int nrMax2 = 0;

            //Primul cel mai mare numar
            for (int i = 0; i < numere.Length; i++)
            {
                if (numere[i] > nrMax)
                {
                    nrMax = numere[i];
                }
            }
            Console.WriteLine($"Primul cel mai mare nr este: {nrMax}");

            //Al doilea numar
            for (int i = 0; i < numere.Length; i++)
            {
                if (numere[i] != nrMax)
                {
                    if (numere[i] > nrMax2)
                    {
                        nrMax2 = numere[i];
                    }
                }
            }
            Console.WriteLine($"Al doilea cel mai mare nr este: {nrMax2}");

            //Aflati elementele comune din cei doi vectori
            int[] v1 = { 2, 4, 67, 1, 3, 56 };
            int[] v2 = { 4, 76, 34, 12, 1, 44, 3, 23 };
            int[] vcom = new int[v1.Length];
            int k = 0;

            for (int i = 0; i < v1.Length; i++)
            {
                for (int j = 0; j < v2.Length; j++)
                {
                    bool apare = false;

                    for (int x = 0; x < vcom.Length; x++)
                    {
                        if (vcom[x] == v2[j])
                        {
                            apare = true;
                        }
                    }
                    if (apare)
                    {
                        continue;
                    }
                    if (v1[i] == v2[j])
                    {
                        vcom[k++] = v2[j];
                    }
                }
            }
            for (int i = 0; i < k; i++)
            {
                Console.WriteLine($"Numere comune : {vcom[i]} ");
            }

            Console.ReadKey();
        }
    }
}
